**Human Like Artificial Agents**

**NOTA**

Behaviours for agents in the game NOTA.

Sandsail:

1. Task Description
    Make Commander unit move the way the wind flows, and selected other units move with the commander
2. Files
    1. Behaviours/commander.json
    2. Behaviours/commander.png - *Image contains art inspired by [other](https://cognigen-cellular.com/explore/wind-clipart-breeze/#gal_post_3388_wind-clipart-breeze-2.png) but drawn separately in PaintDotNet*
    3. Commands/getUnitsPositions.lua
    4. Sensors/findCommander.lua
    5. Sensors/getComPosition.lua
    6. UnitCategories/allelse.json
    7. UnitCategories/commander.json
3. TODOs
    1. Reset move when wind direction is different than current direction
4. Changes
    1. (29.4.2019) Removed unnecessary/unused nodes
    2. (29.4.2019) Changed Common.Move to movement from Formation project
    3. (29.4.2019) Increased wind vector multiplier to 20 instead of 10

CTP2:

1. Task Description
    Make units capture platforms in a given time, while maximizing the score
2. Files
    1. Behaviours/MoveAndCaptureHills.json
    2. Behaviours/MoveAndCaptureHills.png
    3. Commands/gg.lua
    4. Sensors/getHills.lua
    5. Sensors/getScore.lua
    6. UnitCategories/bear.json
    7. UnitCategories/peewees.json
    8. UnitCategories/warriors.json
3. TODOs
    1. Send Bear with warriors to take the last platform
    2. Make widget to autostart the CTP behaviour
    3. Use proper command to end game
4. Changes
    1. Remade the image - *(now it's a tank going up a hill to a flag)*


TTDR:

1. Task Description
    Make scouting units scout enemies, make transport units transport allies back home.
2. Files
    1. Behaviours/cctp.json
    2. Behaviours/cctp.png
    3. Commands/transportAlliesToSafePosition.lua
    4. Commands/scoutEnemyLocations.lua
    6. UnitCategories/transportableUnits.json
    7. UnitCategories/Peepers.json
    8. UnitCategories/atlasses.json
3. TODOs
    1. Save enemies to global variable
    2. Make dynamic path to allies behind enemy lines using the enemies locations saved previously
4. Changes