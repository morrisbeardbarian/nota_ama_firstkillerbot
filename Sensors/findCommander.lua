--- copied from DarioLanza

local sensorInfo = {
	name = "FindCommander",
	desc = "Done for Testing.",
	author = "DarioLanza",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local spGetTooltip = Spring.GetUnitTooltip
return function()

	local myTeamID = Spring.GetMyTeamID()	
	local thisTeamUnits = Spring.GetTeamUnits(myTeamID)
	
	for u=1, #thisTeamUnits do
		local unitID = thisTeamUnits[u]
	 	if spGetTooltip(unitID) == 'Battle Commander - Assault Leader' then
				local bpx, bpy,  bpz =	 Spring.GetUnitPosition(unitID)	
				return {
				x = bpx,
				y = bpy,
				z = bpz,
				} 
	 	end
	end
  
	
	return nill
end