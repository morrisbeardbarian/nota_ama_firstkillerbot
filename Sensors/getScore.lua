function getInfo()
	return {
		--fields = { "score" }
	}
end

return function()
	local missionInfo = Spring.GetGameRulesParam("MissionInfo")
	local missionScore;
	local missionScoreStringTemp = ""
	local mss
	--return string.gmatch(missionInfo, "score=Number#%d+|")
	for word in missionInfo.gmatch(missionInfo,"score=number#%d+\|") do 
		mss = word
		missionScoreStringTemp=word.gsub(word, "score=number#", "")
		missionScoreStringTemp=missionScoreStringTemp.gsub(missionScoreStringTemp, "\|", "")
		missionScore = tonumber(missionScoreStringTemp)
	end
	return {score = missionScore}
end