-- I do this to hardcode a strict line between what is considered to be my territory, and the enemy's territory

local sensorInfo = {
	name = "getUnitsBelowZ",
	desc = "Return the array of units below specified Z.",
	author = "Morris Beardbarian",
	date = "28-04-2019",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local gup = Spring.GetUnitPosition

return function()
	local maxZ = 5100 --HARDCODED Z line
		
	local tid = Spring.GetMyTeamID()
	local allMyUnits = Spring.GetTeamUnits(tid)
	local goodUnits = {}

	for i=1, #allMyUnits do -- Go through each unit
		local tx, ty, tz = gup(allMyUnits[i])
		if(tz <= maxZ) then
			goodUnits[#goodUnits + 1] = allMyUnits[i]
		end
	end
	return goodUnits
end