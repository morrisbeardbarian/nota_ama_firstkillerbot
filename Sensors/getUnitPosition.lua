function getInfo()
	return {
		fields = { "positionX", "positionY", "positionZ" }
	}
end

local getPosition = Spring.GetUnitPosition;

return function(indexOfUnit)

	positionX, positionY, positionZ = getPosition(units[indexOfUnit])

	return {
		positionX = positionX, 
		positionY = positionY, 
		positionZ = positionZ
	}
end