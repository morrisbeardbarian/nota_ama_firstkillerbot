-- I got help from Yulia Malkova with this getHills sensor. She told me her idea and explained what I had to do to get height in a zone (that GetGroundHeight function)

local sensorInfo = {
	name = "detectHills",
	desc = "Return the array of hills with specified height.",
	author = "Morris Beardbarian",
	date = "28-04-2019",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()
	local tileSize = 128 -- HARDCODED VALUE FOR JUST THE EXACT 4 PLATFORMS TO BE RETURNED
	local minimumHeight = 190 --HARDCODED HEIGHT
	local mapX = Game.mapSizeX 
	local mapZ = Game.mapSizeZ
	local mapTilesX = math.ceil(mapX / tileSize) 
	local mapTilesZ = math.ceil(mapZ / tileSize)
	local mapArraySize = mapTilesX * mapTilesZ -- total number of tiles
	local hills = {}
	local hillIndex = 1 
		
	local x = -tileSize --So we can have 0 first
	local z = 0

	for i=1, mapArraySize do -- Go through each tile
		if (i % mapTilesZ == 1) then -- We reset the counter for a new row
			z = 0
			x = x + tileSize
		else
			z = z + tileSize
		end

		hillPositionX = x+tileSize/2
		hillPositionZ = z+tileSize/2
		hillPositionY = Spring.GetGroundHeight(hillPositionX,hillPositionZ) -- Gets the height from this supposed hill
		
		if (hillPositionY > minimumHeight) 
			then 
				hills[hillIndex] = { --put the newfound hill into the array
					x = hillPositionX,
					y = hillPositionY,
					z = hillPositionZ,
				}
				hillIndex = hillIndex+1
			end
		
	end
	return hills
end