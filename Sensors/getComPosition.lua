function getInfo()
	return {
		fields = { "comPositionX", "comPositionY", "comPositionZ" }
	}
end

local getPosition = Spring.GetUnitPosition;

return function()
	if(units.length ~= 1)then
		return {}
	end
	
	-- for i = 1, units.length do
	-- 	centerX, centerY, centerZ = getPosition(units[i])
	-- end

	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = Spring.GetWind()
	-- local ComPosition = getPosition(units[i])

	comCenterX, comCenterY, comCenterZ = getPosition(units[1])

	return {
		comPositionX = comCenterX,
		comPositionY = comCenterY,
		comPositionZ = comCenterZ,
		windDirX = dirX, 
		windDirY = dirY, 
		windDirZ = dirZ, 
		windStr = strength, 
		windNormX = normDirX, 
		windNormY = normDirY, 
		windNormZ = normDirZ
	}
end