function getInfo()
	return {
		onNoUnits = SUCCESS,
		fields = { "comPositionX", "comPositionY", "comPositionZ" },
		parameterDefs = {
			{ 
				name = "comPositionVector",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x=0,z=0}",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			},
			{ 
				name = "spacing",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "5",
			}
		}
	}
end

local getPosition = Spring.GetUnitPosition;
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function PrintTable(tableToPrint, indentationString)
	local nextIndentationString = indentationString

	for key, value in pairs(tableToPrint[1][1]) do
		nextIndentationString = nextIndentationString .. "[" .. key .. "] = " .. tostring(value) .. ","
	end
	Spring.Echo(nextIndentationString)
end

local mt = {} -- create the matrix of positions

function Run(self, units, parameter)
	if(units.length == 0) then
		return {}
	end
	
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local sq = math.floor(math.sqrt(units.length))

    for i=1,sq do
		mt[i] = {} -- create a new row
		for j=1,sq+1 do
			if(j >= sq % 2) then
				local comX = parameter.comPositionVector.x
				local comZ = parameter.comPositionVector.z
				local windX, windY, windZ, strength, windNormDirX, windNormDirY, windNormDirZ = Spring.GetWind()
				local spacing = parameter.spacing
				mt[i][j] = {
							x = (comX - windX) * (spacing + i),
							y = 0, 
							z = (comZ - windZ) * (spacing + j)
						}
			else
				mt[i][j] = {
							x = (comX - windX) * (spacing - i),
							y = 0, 
							z = (comZ - windZ) * (spacing - j)
						}
			end
		end
    end

    local unitRow = 1;
    local unitCol = 1;
	for i = 1, units.length do
		if(unitCol > sq + 1) then
			unitRow=unitRow+1
			unitCol = 1
		end

		-- centerX, centerY, centerZ = getPosition(units[i])

		SpringGiveOrderToUnit(
			units[i], 
			cmdID, 
			Vec3(
				mt[unitRow][unitCol].x, 
				0, 
				mt[unitRow][unitCol].z
				):AsSpringVector(), 
			{})
		unitCol=unitCol+1
	end
	return SUCCESS
end