-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Main CTP Command",
		parameterDefs = {
			{ 
				name = "unitsTable",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			},
			{ 
				name = "hillsTable",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = ""
			}
		}
	}
end

local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function endMission()
	message.SendRules({
		subject = "CTP_playerTriggeredGameEnd",
		data = {},
	})
end

function Run(self, units, parameter)


	local unitsTable = parameter.unitsTable
	local hillsTable = parameter.hillsTable

	local cmdID = CMD.MOVE

	for i=1, #unitsTable do
		if (i > 3) then --- assume there is no more than 3 peewees
			break
		end
		SpringGiveOrderToUnit(unitsTable[i], cmdID, Vec3(hillsTable[i].x, hillsTable[i].y, hillsTable[i].z):AsSpringVector(), {})

	end

	if(Sensors.getScore().score >= 85) then
		endMission()
		return SUCCESS
	else
		return RUNNING
	end
	
end




function Reset(self)
	ClearState(self)
end