function getInfo()
	return {
		onNoUnits = SUCCESS,
		parameterDefs = {
			{ 
				name = "ScoutUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{units}",
			}
		}
	}
end

local gup = Spring.GetUnitPosition;
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function CheckAllOk(myunitsStates, wantedState, myUnits)
	if(myunitsStates[1] == nil) then
		return false
	end
	for i=1,#myunitsStates do
		if( myunitsStates[i] ~= wantedState and Spring.ValidUnitID(myUnitsp[i]) ~= false ) then
			return false
		end
	end
	return true
end

ScoutUnitsStates = {} --Possible states: ScoutPrepare, Scout

local startPos = {}
local goalPos = {}

function Run(self, units, parameter)
	local ScoutUnits = parameter.ScoutUnits
	if(ScoutUnits.length == 0) then
		return {}
	end
	
	local cmdID = CMD.MOVE

	local mapSizeX = Game.mapSizeX
	local mapSizeZ = Game.mapSizeZ
	local mapIncrement = mapSizeX / #ScoutUnits

	if(CheckAllOk(ScoutUnitsStates, "ScoutPrepare")) then
		local onPositions = true
		for i = 1, ScoutUnits.length do
			local tx, ty, tz = gup(ScoutUnits[i])
			if(tx ~= startPos[i].x or ty ~= 10) then
				onPositions = false
			end
		end
		if(onPositions) then
			SpringGiveOrderToUnit(
				units[i], 
				cmdID, 
				Vec3(
					startPos[i].x, 
					0, 
					mapSizeZ - 10
					):AsSpringVector(), 
				{"shift", shift=true})
		end
	else
		for i = 1, ScoutUnits.length do
			startPos[i] = {}
			startPos[i].x = 10 + mapIncrement*(i-1)
			SpringGiveOrderToUnit(
				units[i], 
				cmdID, 
				Vec3(
					startPos[i].x, 
					0, 
					10
					):AsSpringVector(), 
				{"shift", shift=true})
			SpringGiveOrderToUnit(
				units[i], 
				cmdID, 
				Vec3(
					startPos[i].x, 
					0, 
					mapSizeZ - 10
					):AsSpringVector(), 
				{"shift", shift=true})
			ScoutUnitsStates[i] = "ScoutPrepare"
		end
	end
	return RUNNING
end