-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module
local mi = VFS.Include("LuaUI/BETS/projects/core/Sensors/MissionInfo.lua")
attach.Module(modules, "message") -- communication backend load

function getInfo()
	return {
		onNoUnits = SUCCESS,
		parameterDefs = {
			{ 
				name = "TransportUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{units}",
			},
			{ 
				name = "UnitsToTransport",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{units}",
			},
			{
				name = "TargetArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{targetArea}",
			}
		}
	}
end

local gup = Spring.GetUnitPosition;
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function endMission()
	message.SendRules({
		subject = "manualMissionEnd",
		data = {},
	})
end

UnitsStates = {} -- make it global to be saved
unitRandomPlacements = {}

local function RandomPointInCircle(x,y,r)
  r = math.random(0, r) --put random distance from center
  local theta = math.random()*2*math.pi -- random angle
  return { x=x+r*math.cos(theta), y=y+r*math.sin(theta) }
end

local function checkIfUnitsHaveCommands(myUnits)
	for i=1, myUnits.length do
		if(#Spring.GetUnitCommands(myUnits[i], -1) > 0) then
			return true
		end
	end
	return false
end

function Run(self, units, parameter)
	local TransportUnits = parameter.TransportUnits
	local UnitsToTransport = parameter.UnitsToTransport
	local TargetArea = parameter.TargetArea

	-- Transports dead?
	if(TransportUnits.length == 0) then
		return SUCCESS
	end

	-- FINISHED MISSION
	if(mi().score >= 100) then
		endMission()
		return SUCCESS
	end
	
	-- Commands for easier use
	local move = CMD.MOVE
	local load = CMD.LOAD_UNITS
	local unload = CMD.UNLOAD_UNITS
	local wait = CMD.TIMEWAIT

	local executingCommand = false
	for i = 1, TransportUnits.length do
		if(Spring.ValidUnitID(TransportUnits[i]) ~= false) then
			local cmdQueue = Spring.GetUnitCommands(TransportUnits[i], 1);
			if (#cmdQueue>0) then 
				executingCommand = true
			else
				if(UnitsStates[i] == nil and #UnitsToTransport > 0) then
					UnitsStates[i] = "load"
					local unit = UnitsToTransport[1]
					table.remove(UnitsToTransport, 1)
					local x,y,z = gup(unit)
					SpringGiveOrderToUnit(
						TransportUnits[i], 
						load, 
						{ x,y,z, 2}, 
						{"shift"}) --Load Unit
					executingCommand = true
					-- Spring.Echo("LOAD")
				else
					if(UnitsStates[i] == "load") then
						UnitsStates[i] = "moveback"
						local rndInCircle = RandomPointInCircle(TargetArea.center.x, TargetArea.center.z, TargetArea.radius)
						unitRandomPlacements[i] = {x=rndInCircle.x, z=rndInCircle.y}
						SpringGiveOrderToUnit(
							TransportUnits[i], 
							move, 
							{ rndInCircle.x, 0, rndInCircle.y, TargetArea.radius }, 
							{"shift"}) -- Go back
						executingCommand = true
						-- Spring.Echo("MOVE")
					else
						if(UnitsStates[i] == "moveback") then
							UnitsStates[i] = "wait"
							SpringGiveOrderToUnit(
								TransportUnits[i], 
								wait, 
								{ 10 }, 
								{"shift"}) -- Wait
							executingCommand = true
							-- Spring.Echo("WAIT")
						else
							if(UnitsStates[i]) == "wait" then
								UnitsStates[i] = nil
								SpringGiveOrderToUnit(
								TransportUnits[i], 
								unload, 
								{ TargetArea.center.x, 0,TargetArea.center.z, TargetArea.radius}, 
								{"shift"}) -- Unload Unit
								-- Spring.Echo("UNLOAD")
								executingCommand = true
							else
								if(Spring.ValidUnitID(TransportUnits[i]) ~= false) then
									local tx, ty, tz = gup(TransportUnits[i])
									if (#cmdQueue==0 and math.sqrt(tx*tx + tz*tz) > 100) then 
										executingCommand = true
										SpringGiveOrderToUnit(
											TransportUnits[i], 
											move, 
											{ 0, 0, 0 }, -- move to spawn and don't idle on top of another unit
											{"shift"}) -- Go back
										-- Spring.Echo("Done! Going Home!")
									end
								end
							end
						end
					end
				end
			end
		end
	end

	if(checkIfUnitsHaveCommands(TransportUnits) or executingCommand) then
		return RUNNING
	else
		return SUCCESS -- If we haven't given any order, or if we 
	end

	--if(executingCommand) then
--		return RUNNING
	--else 
	--	return SUCCESS
	--end
end